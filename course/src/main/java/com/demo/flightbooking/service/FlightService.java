package com.demo.flightbooking.service;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.demo.flightbooking.entity.Flight;
import com.demo.flightbooking.exception.FlightException;
import com.demo.flightbooking.respository.Flightrespository;
import com.demo.flightbooking.utility.ErrorConstant;

@Service
public class FlightService {

	@Autowired
	Flightrespository flightrepository;
	@Autowired
	EntityManager entityManager;

	public List<Flight> getAllFlight() {

		return flightrepository.findAll();
	}

	public void createFlight(Flight flight) throws FlightException{
		Optional<Flight> list = flightrepository.findByFlightId(flight.getFlightId());
		if(!list.isPresent())
	       {
			flightrepository.save(flight);
			
	       }
		else
		{
			
			throw new FlightException(ErrorConstant.ALREADY_EXISTED);
		}
		
	}

	public void updateFlight(Flight flight) {
		Optional<Flight> flightObject=flightrepository.findByFlightId(flight.getFlightId());
		flightObject.get().setSeatAvailable(flight.getSeatAvailable());
		Session session=entityManager.unwrap(Session.class);
		Transaction transaction=session.beginTransaction();
		session.update(flightObject.get());
		transaction.commit();
	}

	public void deleteByFlightId(String flightid) throws FlightException {
		Optional<Flight> list=flightrepository.findByFlightId(flightid);
		if(!list.isPresent())
	       {
	    	 throw new FlightException(ErrorConstant.NO_RECORD_FOUND);
	       }
		else
		{
			
		 flightrepository.deleteById(list.get().getId());
		}

	}

	public Flight getFlightById(String id) throws FlightException {
		Optional<Flight> flight = flightrepository.findByFlightId(id);
      
		if (!flight.isPresent()) {
			throw new FlightException(ErrorConstant.FLIGHT_NOT_FOUND);
		} else {
			return flight.get();
		}
	}
	
	

}
