package com.demo.flightbooking.utility;

public class ErrorConstant {

	public static final String FLIGHT_NOT_FOUND="flight not found";
	public static final String NO_RECORD_FOUND="no record found";
	public static final String ALREADY_EXISTED="Already flight is existed";
	
}
