package com.demo.flightbooking.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class GlobalException extends ResponseEntityExceptionHandler {
	
	@ExceptionHandler(FlightException.class)
	public ResponseEntity<ErrorResponse> error(FlightException ex)
	{
		ErrorResponse er=new ErrorResponse();
		
		er.setMessage(ex.getMessage());

		//er.setStatus(HttpStatus.NOT_FOUND.value());
		
		
		return new ResponseEntity<ErrorResponse>(er,HttpStatus.NOT_ACCEPTABLE);
	

}

}