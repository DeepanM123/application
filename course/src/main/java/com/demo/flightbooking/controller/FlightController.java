package com.demo.flightbooking.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.demo.flightbooking.entity.Flight;
import com.demo.flightbooking.entity.FlightDto;
import com.demo.flightbooking.exception.FlightException;
import com.demo.flightbooking.service.FlightService;
import com.fasterxml.jackson.databind.util.BeanUtil;

@RestController
public class FlightController {

	// @Autowired
	// service service;

	@Autowired
	FlightService flightservice;

	@GetMapping("/getFlightDetails")
	private List<Flight> getAllFlights() {
		return flightservice.getAllFlight();
	}

	// create-Post
	@PostMapping("/createFlight")
	public ResponseEntity<String> createFlight(@RequestBody FlightDto flightDto) throws FlightException, ParseException {

		SimpleDateFormat format=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		//System.out.println(flightDto.getDate());
		Date requiredDate=format.parse(flightDto.getDate());
		//System.out.println(RequiredDate);
		Flight flight=new Flight();
		flight.setDate(requiredDate);
		BeanUtils.copyProperties(flightDto, flight);
		
		flightservice.createFlight(flight);
		//System.out.println(); 
		return new ResponseEntity<String>("registered",HttpStatus.CREATED);
	}

	// update-Put
	@PutMapping("/updateFlightDetails")
	public Flight updateFlightDetails(@RequestBody Flight flight) {
		flightservice.updateFlight(flight);
		return flight;
	}

	// Delete-DELETE
	@DeleteMapping("/deleteFlight/{flightId}")
	public ResponseEntity<String> deleteFlight(@RequestParam("flightId") String flightid) throws FlightException {
		flightservice.deleteByFlightId(flightid);
		return new ResponseEntity<String>("deleted", HttpStatus.ACCEPTED);
	}

	@GetMapping("/flight/{flight_id}")
	public ResponseEntity<Flight> findbyflightid(@RequestParam String id) throws FlightException {

		Flight flight = flightservice.getFlightById(id);

		return new ResponseEntity<Flight>(flight, HttpStatus.OK);
	}

}
