package com.demo.flightbooking.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
@Entity
public class Passenger implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@Column(unique = true)
	private String email;
	private boolean loggedIn;
	private String password;
	@OneToMany(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH,
			CascadeType.REFRESH, CascadeType.REMOVE }, mappedBy = "passenger")
	List<PassengerFlight> passengerFligt;
	@Override
	public String toString() {
		return "Passenger [id=" + id + ", email=" + email + ", loggedIn=" + loggedIn + ", password=" + password
				+ ", passengerFligt=" + passengerFligt + "]";
	}
	
	

}
