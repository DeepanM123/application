package com.demo.flightbooking.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
@Entity
public class PassengerFlight implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private int ticketBooked;
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateTime;
     
     @ManyToOne(cascade = CascadeType.ALL)
 	private Passenger passenger;
 	@ManyToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH })
 	private Flight flight;
 	@OneToMany(cascade = CascadeType.ALL)
 	@JoinColumn(name = "co_passenger")
 	List<FlightDetail> flightDetail;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public int getTicketBooked() {
		return ticketBooked;
	}
	public void setTicketBooked(int ticketBooked) {
		this.ticketBooked = ticketBooked;
	}
	public Date getDateTime() {
		return dateTime;
	}
	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}
	public Passenger getPassenger() {
		return passenger;
	}
	public void setPassenger(Passenger passenger) {
		this.passenger = passenger;
	}
	public Flight getFlight() {
		return flight;
	}
	public void setFlight(Flight flight) {
		this.flight = flight;
	}
	public List<FlightDetail> getFlightDetail() {
		return flightDetail;
	}
	public void setFlightDetail(List<FlightDetail> flightDetail) {
		this.flightDetail = flightDetail;
	}
 	
}
