package com.demo.flightbooking.respository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.demo.flightbooking.entity.Flight;

public interface Flightrespository extends JpaRepository<Flight, Integer> {

	Optional<Flight> findByFlightId(String flightid);
	//List<Flight> Flight_Id(String id);

	Flight deleteByFlightId(String flightId);
}
